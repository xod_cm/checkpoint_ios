//
//  LoginViewController.m
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"

@interface LoginViewController ()
{
    BOOL isUp;
}
@end

@implementation LoginViewController

@synthesize labelLogin = _labelLogin, labelPassword = _labelPassword, log = _log, login = _login, password = _password;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed: @"home_0004_checkpoint-"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: image];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITextfieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGRect bounds = [[self view] frame];
    float b = 216.0 - ([[self view] frame].size.height - (textField.frame.origin.y + textField.frame.size.height));
    if (b > 0.0)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelay:0.2];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        isUp = YES;
        bounds.origin.y = b * -1;
        [[self view] setFrame: bounds];
        [UIView commitAnimations];
    }
    
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (isUp)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.1];
        [UIView setAnimationDelay:0.2];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        CGRect bounds = [[self view] frame];
        bounds.origin.y = 0;
        [[self view] setFrame: bounds];
        [UIView commitAnimations];
        
        isUp = NO;
    }
    else
    {
        [textField resignFirstResponder];
        [_password becomeFirstResponder];
    }

    return YES;
}


#pragma IBAction

- (IBAction)pushLogAction:(id)sender
{
    HomeViewController *controller =  [[self storyboard] instantiateViewControllerWithIdentifier:@"homeViewController"];
    [[self navigationController] pushViewController: controller animated: YES];
}

@end
