//
//  ActivitySelectionViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 10/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CreateEventViewController;

@interface ActivitySelectionViewController : UITableViewController
{
    NSArray *activities;
    
    CreateEventViewController *parent; //degeux
}

@property (nonatomic, strong) CreateEventViewController *parent;
@property (nonatomic, retain) NSArray *activities;
@property (nonatomic, retain) NSString *selected;

@end
