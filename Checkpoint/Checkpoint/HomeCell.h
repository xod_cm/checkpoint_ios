//
//  HomeCell.h
//  Checkpoint
//
//  Created by Morgan Collino on 09/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UITableViewCell
{
    UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

@end
