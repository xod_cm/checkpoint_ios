//
//  HomeViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIButton *rer;
    IBOutlet UIButton *metro;
    IBOutlet UIButton *bus;
    IBOutlet UIButton *t;
    IBOutlet UIButton *n;
    IBOutlet UIButton *v;
    
    IBOutlet UIButton *ownEvents;
    IBOutlet UIButton *createEvent;
    
    IBOutlet UIButton *returnButton;
    
    IBOutlet UIImageView *roundRect;
    IBOutlet UIImageView *titleImage;
    
    IBOutlet UITableView *tableContactView;
}

@property (nonatomic, strong) UIButton *rer;
@property (nonatomic, strong) UIButton *metro;
@property (nonatomic, strong) UIButton *bus;
@property (nonatomic, strong) UIButton *t;
@property (nonatomic, strong) UIButton *n;
@property (nonatomic, strong) UIButton *v;

@property (nonatomic, strong) UIButton *ownEvents;
@property (nonatomic, strong) UIButton *createEvent;

@property (nonatomic, strong) UIImageView *roundRect;

@property (nonatomic, strong) UITableView *tableContactView;


@end
