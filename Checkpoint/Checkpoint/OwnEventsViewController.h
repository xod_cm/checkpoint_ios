//
//  OwnEventsViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnEventsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *mTableView;
    UILabel  *label;
}

@property (nonatomic, retain) IBOutlet UITableView *mTableView;
@property (nonatomic, retain) IBOutlet UILabel *label;

@end
