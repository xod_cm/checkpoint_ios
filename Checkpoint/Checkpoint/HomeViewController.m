//
//  HomeViewController.m
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "HomeViewController.h"
#import "OwnEventsViewController.h"
#import "EventsViewController.h"
#import "HomeCell.h"



@interface HomeViewController ()
{
    NSMutableDictionary *contacts;
    NSArray *keys;
    NSMutableArray *selected;
    NSMutableArray *selectedName;
}


@end

@implementation HomeViewController

@synthesize rer = _rer, metro = _metro, bus = _bus, t = _t, n = _n, v = _v, ownEvents = _ownEvents, createEvent = _createEvent, roundRect = _roundRect, tableContactView = _tableContactView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    contacts = nil;
    UIImage *image = [UIImage imageNamed: @"home_0004_checkpoint-"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: image];

    // test
    contacts = [[NSMutableDictionary alloc] init];
    [contacts setObject: @[@"Armand", @"Arnaud", @"Arthur"] forKey:@"a"];
    [contacts setObject: @[@"Bernard", @"Bertrand", @"Blaise"] forKey:@"b"];
    [contacts setObject: @[@"David", @"Dodo", @"Dupuis"] forKey:@"d"];
    [contacts setObject: @[@"Maman", @"Mohammed", @"Moktar"] forKey:@"m"];
    
    selected = [[NSMutableArray alloc] init];
    selectedName = [[NSMutableArray alloc] init];
    
    keys = [[contacts allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)rerButton:(id)sender
{
    NSLog(@"rerButton");
}

- (IBAction)metroButton:(id)sender
{
    NSLog(@"metroButton");
}

- (IBAction)busButton:(id)sender
{
    NSLog(@"busButton");
}

- (IBAction)tButton:(id)sender
{
    NSLog(@"tButton");
}

- (IBAction)nButton:(id)sender
{
    NSLog(@"nButton");
}

- (IBAction)vButton:(id)sender
{
    NSLog(@"vButton");
}


- (IBAction)createEventButton:(id)sender
{
    for (NSIndexPath *indexPath in selected)
    {
        NSString *key = [keys objectAtIndex: indexPath.section];
        NSArray *array = [contacts objectForKey: key];
        NSString *value = [array objectAtIndex: indexPath.row];
        [selectedName addObject: value];
    }
    
    NSLog(@"SelectedName:%@", selectedName);
    [selectedName removeAllObjects];
    
    NSLog(@"createEventButton");
    OwnEventsViewController *controller =  [[self storyboard] instantiateViewControllerWithIdentifier:@"createEventViewController"];
    [[self navigationController] pushViewController: controller animated: YES];
}


- (IBAction)ownEventsButton:(id)sender
{
    NSLog(@"ownEventsButton");
    OwnEventsViewController *controller =  [[self storyboard] instantiateViewControllerWithIdentifier:@"ownEventsViewController"];
    [[self navigationController] pushViewController: controller animated: YES];
}


#pragma UITableViewDatasourceAndDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell = (HomeCell *)[tableView cellForRowAtIndexPath: indexPath];
    
    
    if (![selected containsObject: indexPath])
    {
        [selected addObject: indexPath];
        [cell setAccessoryType: UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [selected removeObject: indexPath];
        [cell setAccessoryType: UITableViewCellAccessoryNone];
    }
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell = (HomeCell *)[tableView dequeueReusableCellWithIdentifier: @"HomeCell"];
    if (cell == nil)
    {
        cell = [[HomeCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"HomeCell"];
    }
    int section = indexPath.section;
    int row = indexPath.row;
    NSString *key = [keys objectAtIndex: section];

    if ([selected containsObject: indexPath])
    {
        [cell setAccessoryType: UITableViewCellAccessoryCheckmark];
    }
    else
        [cell setAccessoryType: UITableViewCellAccessoryNone];

    if (key)
    {
        NSArray *array = [contacts objectForKey: key];
        [[cell label] setText: [array objectAtIndex: row]];
    }
    return cell;
}


/*
 
    NSDictionary:
 
        {
            "a": [@"adrien", @"arnaud",...],
            "b": [@"bertrand", @"bastien", ..],
            "d": [@"david", @"dede", ...]
        }
 
 */

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (contacts != nil)
    {
        NSString *key = [keys objectAtIndex: section];    
        NSArray *array = [contacts objectForKey: key];
        
        return [array count];
    }
    return 0;
}

- (int) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[contacts allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *key = [keys objectAtIndex: section];
    return (key);
}


@end
