//
//  HomeCell.m
//  Checkpoint
//
//  Created by Morgan Collino on 09/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

@synthesize label = _label;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
