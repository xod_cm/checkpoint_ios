//
//  EventsViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    BOOL eventResponded;
    
    UIButton *yes;
    UIButton *no;

    UIImageView *imageEvent;
    UITableView *guestTableView;
    UILabel *labelTable;
    UILabel *nameLabel;
    UILabel *quoteLabel;
    
    // Hidden
    UILabel *acceptLabel;
    UIButton *itineraire;
    UIButton *cancel;
    
    
    NSArray *listPeople;
}

@property (nonatomic) BOOL eventResponded;
@property (nonatomic, retain) NSArray *listPeople;

@property (nonatomic) IBOutlet UIButton *yes;
@property (nonatomic) IBOutlet UIButton *no;

@property (nonatomic) IBOutlet UIImageView *imageEvent;
@property (nonatomic) IBOutlet UITableView *guestTableView;
@property (nonatomic) IBOutlet UILabel *labelTable;
@property (nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic) IBOutlet UILabel *quoteLabel;

// Hidden
@property (nonatomic) IBOutlet UILabel *acceptLabel;
@property (nonatomic) IBOutlet UIButton *itineraire;
@property (nonatomic) IBOutlet UIButton *cancel;

- (void) toRespondedMode;

@end
