//
//  EventsViewController.m
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "EventsViewController.h"

@interface EventsViewController ()

@end

@implementation EventsViewController

@synthesize yes = _yes, no = _no, imageEvent = _imageEvent, guestTableView = _guestTableView, labelTable = _labelTable, nameLabel = _nameLabel, quoteLabel = _quoteLabel, acceptLabel = _acceptLabel, itineraire = _itineraire, cancel = _cancel, listPeople = _listPeople, eventResponded = _eventResponded;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed: @"home_0004_checkpoint-"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: image];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listPeople count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ActivityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    //Guest *guest = [listPeople objectAtIndex: indexPath.row];
    
    [[cell textLabel] setText: @"test"];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


- (void) toRespondedMode
{    
    _eventResponded = YES;
    _yes.hidden = YES;
    _yes.enabled = YES;
    _no.hidden = YES;
    _no.enabled = YES;
    _cancel.hidden = NO;
    _itineraire.hidden = NO;
    _acceptLabel.hidden = NO;
    _nameLabel.hidden = YES;
    _quoteLabel.hidden = YES;
    
    [_imageEvent setImage: [UIImage imageNamed: @"home_0015_réponse-envoyée"]];
}


#pragma UIButtonMethod

- (IBAction)respondYes:(id)sender
{
    [self toRespondedMode];
    //send yes to event
}


- (IBAction)respondNo:(id)sender
{
    [self toRespondedMode];
    //send no to event
}

- (IBAction)toItineraire:(id)sender
{
    
}


- (IBAction)toCancel:(id)sender
{
    
}

@end
