//
//  CreateEventViewController.m
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "CreateEventViewController.h"
#import "ActivitySelectionViewController.h"
#import "DateSelectionViewController.h"

@interface CreateEventViewController ()
{
    NSArray *arrayActivity;
}
@end


@implementation CreateEventViewController

@synthesize activityButton = _activityButton, dateButton = _dateButton, launchRequest = _launchRequest, activity = _activity, dateSelected = _dateSelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrayActivity = [[NSArray alloc] initWithObjects:@"Manger", nil];

    _activity = [[NSString alloc] init];
    _dateSelected = [[NSDate alloc] initWithTimeIntervalSinceNow: 0];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    UIImage *image = [UIImage imageNamed: @"home_0004_checkpoint-"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: image];
    
    NSLog(@"Date: %@", _dateSelected);
    NSLog(@"Activity: %@", _activity);
    
    
    if (_activity) // set button name
        [_activityButton setTitle: _activity forState:UIControlStateNormal];
    if (_dateSelected)
    {
        [_dateButton setTitle: @"date" forState: UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)popActivities:(id)sender
{
    ActivitySelectionViewController *controller =  [[self storyboard] instantiateViewControllerWithIdentifier:@"activitySelectionViewController"];
    [controller setParent: self];    
    [controller setActivities: arrayActivity];
    [[self navigationController] pushViewController: controller animated: YES];
}

- (IBAction)popDate:(id)sender
{
    DateSelectionViewController *controller =  [[self storyboard] instantiateViewControllerWithIdentifier:@"dateSelectionViewController"];
    [controller setParent: self];
    [[self navigationController] pushViewController: controller animated: YES];
}

- (IBAction)launchInvit:(id)sender
{
    [[self navigationController] popViewControllerAnimated: YES];
}
@end
