//
//  DateSelectionViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 11/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CreateEventViewController;

@interface DateSelectionViewController : UIViewController
{
    NSDate *dateSelected;
    UIDatePicker *datePicker;
    CreateEventViewController *parent; //degeux
}

@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) CreateEventViewController *parent;

@end
