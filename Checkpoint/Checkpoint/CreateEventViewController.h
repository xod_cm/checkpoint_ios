//
//  CreateEventViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEventViewController : UIViewController
{
    UIButton *activityButton;
    UIButton *dateButton;
    UIButton *launchRequest;
    
    NSString *activity;
    NSDate   *dateSelected;
}

- (IBAction)launchInvit:(id)sender;

@property (nonatomic) NSString *activity;
@property (nonatomic) NSDate   *dateSelected;
@property (nonatomic, retain) IBOutlet UIButton *activityButton;
@property (nonatomic, retain) IBOutlet UIButton *dateButton;
@property (nonatomic, retain) IBOutlet UIButton *launchRequest;


@end
