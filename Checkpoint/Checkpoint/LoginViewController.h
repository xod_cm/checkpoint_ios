//
//  LoginViewController.h
//  Checkpoint
//
//  Created by Morgan Collino on 04/06/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *login;
    IBOutlet UITextField *password;
    IBOutlet UILabel *labelLogin;
    IBOutlet UILabel *labelPassword;
    IBOutlet UIButton *log;
}

@property (nonatomic, retain) UITextField *login;
@property (nonatomic, retain) UITextField *password;
@property (nonatomic, retain) UILabel *labelLogin;
@property (nonatomic, retain) UILabel *labelPassword;
@property (nonatomic, retain) UIButton *log;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
